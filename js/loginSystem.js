
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
    apiKey: "AIzaSyBM1aJ6fQKEl5Nb3IClguEYJNPAsO8XUPw",
    authDomain: "cultkostechheroes2.firebaseapp.com",
    projectId: "cultkostechheroes2",
    storageBucket: "cultkostechheroes2.appspot.com",
    messagingSenderId: "365331157776",
    appId: "1:365331157776:web:c794652e8c877f8889cb95",
    measurementId: "G-4G4BVS8LM9"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

const auth = firebase.auth();
const db = firebase.firestore();


console.log("firesorte")
let signUpBtn = document.getElementById("signUpButton");
let signUpEmailInput = document.getElementById("signUpEmail");
let signUpPasswordInput = document.getElementById("signUpPassword");
let signUpName = document.getElementById("signUpName");
let signUpTown = document.getElementById("signUpTown");
let logoutLink = document.getElementById("signOut");
let signInEmail = document.getElementById("signInEmail");
let signInPassword = document.getElementById("signInPassword");
let signInButton = document.getElementById("signInButton");
let modalButton = document.getElementById("modalButton")
let modalDate = document.getElementById("modalDate");
let modalDsp = document.getElementById("modalDsp");
let modalCity = document.getElementById("modalCity");
let modalCategory = document.getElementById("modalCategory");
auth.onAuthStateChanged((user) => {
    if (user) {
        window.localStorage.setItem('user', JSON.stringify(user));
        console.log(user.displayName);
        // showUI();
    } else {
        console.log("No user is logged in");
        // hideUI();
    }
});
signUpBtn.addEventListener("click", () => {
    console.log(1)
    auth.createUserWithEmailAndPassword(signUpEmailInput.value, signUpPasswordInput.value)
        .then((users) => {
            window.location.replace("http://127.0.0.1:5500/index.html");
            return db.collection("users")
                .doc(users.user.uid)
                .set({
                    city: signUpBtn.value,
                })
                .then(() => {
                    users.user.updateProfile({
                        displayName: signUpName.value
                    })
                    signUpEmailInput.value = "";
                    signUpPasswordInput.value = "";
                    signUpName.value = "";
                    signUpTown.value = "";
                })

        });
    console.log(signUpName.value)
});
// window.localStorage.setItem('user', JSON.stringify(user));


signInButton.addEventListener('click', () => {
    auth.signInWithEmailAndPassword(signInEmail.value, signInPassword.value)
        .then(() => {
            signInEmail.value = "";
            signInPassword.value = "";
            window.location.replace("http://127.0.0.1:5500/index.html");

        }).catch(
            (error) => {
                if (error.code == "auth/wrong-password") {
                    document.getElementById("error").style.display = "block";
                }
                setTimeout(() => {
                    document.getElementById("error").style.display = "none";

                }, 2000);
            })
})

if (logoutLink == null) {

} else {

    logoutLink.addEventListener("click", () => {
        auth.signOut();
        console.log("asda")
    });
}


function addEventi(category, dsp, city, date) {
    const unique = firebase.auth().currentUser.uid;
    return db
        .collection("events")
        .doc(unique)
        .set({
            username: JSON.parse(window.localStorage.getItem("user")).displayName,
            category: category,
            dsp: dsp,
            city: city,
            date: date
        }).then(() => {
            //function to get data from event
        })
}


if (modalButton == null) {

} else {
    modalButton.addEventListener('click', () => {
        addEvent(modalCategory.value,
            modalDsp.value, modalCity.value, modalDate.value);
        console.log("a")
    })

}

