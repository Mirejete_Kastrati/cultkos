
var firebaseConfig = {
    apiKey: "AIzaSyBM1aJ6fQKEl5Nb3IClguEYJNPAsO8XUPw",
    authDomain: "cultkostechheroes2.firebaseapp.com",
    projectId: "cultkostechheroes2",
    storageBucket: "cultkostechheroes2.appspot.com",
    messagingSenderId: "365331157776",
    appId: "1:365331157776:web:c794652e8c877f8889cb95",
    measurementId: "G-4G4BVS8LM9"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

const auth = firebase.auth();
const db = firebase.firestore();
let loginIcon = document.getElementById("navListLogin");
let logOut = document.getElementById("logOut");
let logOutButton = document.getElementById("logOutButton");
let userIcon = document.getElementById("usericon");
let userContent = document.getElementById("userContent");
let addEventButton = document.getElementById("addEvent");
loginIcon.classList.add("hidden")
logOut.classList.add("hidden");
userIcon.classList.add("hidden");
if (addEventButton == null) {

} else {
    addEventButton.classList.add("hidden")
}
function showUI() {
    loginIcon.classList.add("hidden")
    logOut.classList.remove("hidden");
    userIcon.classList.remove("hidden");
    if (document.getElementById("addEvent") == null) {

    } else {
        document.getElementById("addEvent").classList.remove("hidden")
    }
}
function hideUI() {
    loginIcon.classList.remove("hidden")
    logOut.classList.add("hidden");
    userIcon.classList.add("hidden");
    if (document.getElementById("addEvent") == null) {

    } else {
        document.getElementById("addEvent").classList.add("hidden")
    }
    // s
}

let user;
function getDataFromLocalStorage() {
    if (JSON.parse(window.localStorage.getItem('user')) !== null) {
        user = JSON.parse(window.localStorage.getItem("user"));
        userContent.innerHTML = user.displayName;
    } else {
        userContent.innerHTML = "";
        console.log(1)
    }
};
auth.onAuthStateChanged((user) => {
    if (user) {
        window.localStorage.setItem('user', JSON.stringify(user));
        // console.log(user);
        getDataFromLocalStorage();
        showUI();
    } else {
        window.localStorage.setItem('user', null);
        // console.log("No user is logged in from app");
        getDataFromLocalStorage();
        hideUI();
    }
});

//logout function
logOutButton.addEventListener("click", () => {
    auth.signOut();
    getDataFromLocalStorage();
})

//function to add event to firestore
function addEventi(category, dsp, city, date) {
    const unique = firebase.auth().currentUser.uid;
    return db
        .collection("events")
        .doc(unique)
        .set({
            username: JSON.parse(window.localStorage.getItem("user")).displayName,
            category: category,
            dsp: dsp,
            city: city,
            date: date
        }).then(() => {
            addEventContent(JSON.parse(window.localStorage.getItem("user")).displayName, category, dsp, city, date)
        })
}

//modal handler
let modalButton = document.getElementById("modalButton")
if (modalButton == null) {

} else {
    modalButton.addEventListener('click', () => {
        addEventi(modalCategory.value,
            modalDsp.value, modalCity.value, modalDate.value);
        $('#exampleModal').modal('toggle');
    })
}

//function to add event on document.
function addEventContent(username, category, dsp, city, date) {
    let containerCard = document.getElementsByClassName("cards-wrapper")[0];
    let grid = document.createElement("div");
    grid.className = "card-grid-space";
    let card = document.createElement("a");
    card.className = "card";
    let div = document.createElement("div");
    // div.className = "date"
    let h1 = document.createElement("h1");
    h1.innerHTML = category;
    let p = document.createElement("p");
    p.innerHTML = dsp;
    let dateC = document.createElement("div");
    dateC.className = "date"
    dateC.innerHTML = username + "<br>" + city + "<br>" + date;
    div.appendChild(h1);
    div.appendChild(p);
    div.appendChild(dateC);
    card.appendChild(div);
    grid.appendChild(card);
    containerCard.appendChild(grid);
}

